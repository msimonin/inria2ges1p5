#!/usr/bin/python3

import getopt
import sys
import csv
import re


DELIMITER = ";"
MODE_DE_TRANSPORT = "Type de transporteur"
IN_FLIGHT = "Air"
OUT_FLIGHT = "Avion"
IN_TRAIN = "Train"
OUT_TRAIN = "Train"

ORIGIN_CITY = "Lieu départ"
ORIGIN_CITY_CODE = "Code lieu départ"
ORIGIN_COUNTRY_CODE = "Code pays départ"
DESTINATION_CITY_CODE = "Code lieu destination"
DESTINATION_CITY = "Lieu destination"
DESTINATION_COUNTRY_CODE = "Code pays destination"

CODE_VOYAGE = "Code Voyage"

def usage():
    print("""Usage: oreli_to_l1p5.py [-h | --help] [-m] [-f filter_filename] -y year -i input_filename -o output_filename

Converts an ORELI table in CSV to the labo1point5 format.

Available options:
  -h --help              displays this message
  -y year                year of the extraction
  -i input_filename      specifies the path of the Oreli file exported in CSV format using either Excel or LibreOffice
  -f filter_filename     specifies the path of the filter file of 'Libellé structure bénéficiaire' (one name by row)
  -m                     without writing motif of the mission
  -o output_filename     specifies the path of the output labo1point5 file
""")

def main(argv):
    try:
        opts, args = getopt.getopt(argv, "hi:o:y:f:mi:", ["help"])
    except getopt.GetoptError as e:
        print('Error: ' + e.msg + '\n')
        usage()
        sys.exit(1)
    input_filename = None
    output_filename = None
    year = None
    filter_structures_filename = None
    col_motif = True
    for opt, arg in opts:
        if opt in ('-h','--help'):
            usage()
            sys.exit()
        elif opt in ('-i'):
            input_filename = arg
        elif opt in ('-o'):
            output_filename = arg
        elif opt in ('-y'):
            year = arg 
        elif opt in ('-f'):
            filter_structures_filename = arg
        elif opt in ('-m'):
            col_motif = False
    if not input_filename:
        print('The input filename is mandatory. Specify it with -i')
        sys.exit()
    if not output_filename:
        print('The output filename is mandatory. Specify it with -o')
        sys.exit()
    if not year:
        print('The year is mandatory. Specify it with -y')
        sys.exit()

    # ident,type,name,elevation_ft,continent,iso_country,iso_region,municipality,gps_code,iata_code,local_code,coordinates
    with open('airport-codes_csv.csv', encoding='utf8') as file:
        airport_codes = list(csv.DictReader(file, delimiter=','))

    def priority(row):
        e = row['type']
        if e == 'large_airport':
            return 100
        elif e == 'medium_airport':
            return 75
        elif e == 'small_airport':
            return 50
        elif e == 'heliport':
            return 25
        elif e == 'seaplane_base':
            return 15
        elif e == 'closed':
            return 5
        else:
            return 1

    MAXPRIORITY = 9999999
    fallback_map = {
        'st gervais le fayet': {'name':'St-Gervais Le Fayet','iso country':'FR', 'priority': MAXPRIORITY},
        'creusot montceau mont': {'name':'Le Creusot - Montceau TGV','iso country':'FR', 'priority': MAXPRIORITY},
        'paris disneyland': {'name':'Paris','iso country':'FR', 'priority': MAXPRIORITY},
        'paris gare montparnasse': {'name':'Paris','iso country':'FR', 'priority': MAXPRIORITY},
        'massy tgv train station': {'name':'Paris','iso country':'FR', 'priority': MAXPRIORITY},
        'st malo gare': {'name': 'Saint Malo', 'iso country': 'FR', 'priority': MAXPRIORITY},
        'nantes railway': {'name': 'Nantes', 'iso country': 'FR', 'priority': MAXPRIORITY},
        'brest(france)': {'name': 'Brest', 'iso country': 'FR', 'priority': MAXPRIORITY},
        'laval': {'name': 'Laval', 'iso country': 'FR', 'priority': MAXPRIORITY},
        'aix en provence tgv': {'name': 'Aix en Provence', 'iso country': 'FR', 'priority': MAXPRIORITY},
        'st raphael': {'name': 'Saint Raphael', 'iso country': 'FR', 'priority': MAXPRIORITY},
        'lorraine tgv': {'name': 'Nancy', 'iso country': 'FR', 'priority': MAXPRIORITY},
        'granville': {'name': 'Nancy', 'iso country': 'FR', 'priority': MAXPRIORITY},
        'geneva': {'name': 'Genève', 'iso country': 'CH', 'priority': MAXPRIORITY},
        'marne la vallee chessy' : {'name': 'Marne La Vallée', 'iso country': 'FR', 'priority': MAXPRIORITY},
        'saarbrücken main station': {'name': 'Sarrebruck', 'iso country': 'DE', 'priority': MAXPRIORITY},
        'schoena': {'name':'Schöna','iso country':'DE', 'priority': MAXPRIORITY},
        'default': {'name':'default','iso country':'', 'priority': MAXPRIORITY}
    }

    airport_codes_map = {}
    city_map = fallback_map
    def insert_cityname1(name, data):
        key = name.lower().replace('ü','u')
        if key in city_map:
            other = city_map[key]
            if data['priority'] > other['priority']:
                city_map[key] = data
        else:        
            # print('add ',row['municipality'])
            city_map[key] = data

    def insert_cityname(name, data):
        insert_cityname1(name, data)
        data2 = data.copy()
        data2['priority'] = data2['priority']-1
        if '-' in name:
            insert_cityname1(name.replace('-',' '), data2)
        if ' (' in name:
            insert_cityname1(name.split(' (')[0], data2)
        if 'Saint-' in name:
            insert_cityname1(name.replace('Saint-','st '), data2)
    
    for row in airport_codes:
        if row['iata_code']:
            airport_codes_map[row['iata_code']] = row
        if row['municipality']:
            data = {
                'priority': priority(row),
                'name': row['municipality'],
                'iso country': row['iso_country']
            }
            insert_cityname(row['municipality'], data)
            for subname in row['municipality'].split('/'):
                insert_cityname(subname, data)

    with open('cities500.txt', encoding='utf8') as file:
        for geonameid,name,asciiname,alternatenames,latitude,longitude,feature_class,feature_code,country_code,cc2,admin1_code,admin2_code,admin3_code,admin4_code,population,elevation,dem,timezone,modification_date in csv.reader(file, delimiter='\t'):
            data = {
                'priority': int(population)+150,
                'name': name,
                'iso country': country_code
            }
            insert_cityname(name, data)
            insert_cityname(asciiname, data)

    # overwrite/add some unknown railway station

    date = '01/01/{}'.format(year)

    def l1p5mode(in_mode):
        if in_mode == IN_FLIGHT:
            return OUT_FLIGHT
        if in_mode == IN_TRAIN:
            return OUT_TRAIN
        print('❌ mode de transport invalide')
        raise Exception(f"Mode de transport invalide {in_mode}")

    def mode(row):
        try:
            if MODE_DE_TRANSPORT in row:
                return l1p5mode(row[MODE_DE_TRANSPORT])
            else:
                if row['Rail Emission Factor (g/Km)']!='\'-':
                    return 'Train'
                else:
                    return 'Avion'
        except Exception as e:
            print(f'❌ mode de transport invalide for {row}')
            raise e

    def cleanup_oreli_cityname(name):
        return name.lower().split(' (')[0].split('/')[0]
    
    def test_by_splitting(cityname):
        # split on ' ' (can be improved)
        def test_splitted(splitted):
            for i in range(0, len(splitted)):
                # du plu sprécis ou plus vague
                _cityname = " ".join(splitted[0:len(splitted)-i])
                if _cityname in city_map:
                    return city_map[_cityname], _cityname
            return (None, None)
        data, candidate = test_splitted(cityname.split(" "))
        if data is not None:
            return data, candidate

        data, candidate = test_splitted(cityname.split("-"))
        if data is not None:
            return data, candidate
        return None, None

    # returns the country code and cleaned-up city name
    def process_city(citycode, a_cityname, in_data_county_code, mode):
        data = None
        cityname = cleanup_oreli_cityname(a_cityname)

        # lookup in the airport list
        if mode == IN_FLIGHT and citycode in airport_codes_map:
            data = airport_codes_map[citycode]
            # sanity check in_data_citycode =? computed iso_country
            if in_data_county_code.upper().strip() != data['iso_country'].upper().strip():
                print(f"⚠️ {citycode} - {a_cityname} - {in_data_county_code} - {mode}")
                print(f"| in data {in_data_county_code} differs from looked up one {data['iso_country']}")
            return {'iso country': data['iso_country'], 'name': data['municipality']}

        data, candidate = test_by_splitting(cityname)
        if data is not None:
            print(f"ℹ️ {citycode} - {a_cityname} - {in_data_county_code} - {mode}")
            print(f"| {cityname} changed to {candidate} -> {data}")

            if in_data_county_code.upper().strip() != data['iso country'].upper().strip():
                print(f"⚠️ {citycode} - {a_cityname} - {in_data_county_code} - {mode}")
                print(f"| in data {in_data_county_code} differs from looked up one {data['iso country']}")

            return data       
        else:
            print('❌ City not found: ', citycode, ' - ', a_cityname, ' (', cityname, ')')
            return {'iso country':'', 'name': cityname}

    def is_two_way(row):
        if ( (('Type de trajet' in row) and row['Type de trajet']=='Aller retour') or
             (('A/R' in row) and row['A/R']== 'Aller retour') ):
            return 'OUI'
        else:
            return 'NON'
    
    with open(input_filename, encoding='utf8') as file:
        oreli_in = list(csv.DictReader(file, delimiter=DELIMITER))

    filter_structures = {}
    if filter_structures_filename != None:
        with open(filter_structures_filename, encoding='utf8') as file:
            list_structures = list(csv.reader(file, delimiter=','))
            filter_structures = [row1[0] for row1 in list_structures]
    
    oreli_out = []

    for row in oreli_in:
        row_mode = mode(row)
        if (filter_structures_filename is None) or (row['Libellé structure bénéficiaire'] in filter_structures):
            id = ''
            if 'Customer Defined 02' in row:
                id = row['Customer Defined 02']
            if "Numéro d'OM" in row:
                id = row["Numéro d'OM"]
            if CODE_VOYAGE in row:
                id = row[CODE_VOYAGE]
            if id == '':
                id = '000000'
            src = process_city(row[ORIGIN_CITY_CODE], row[ORIGIN_CITY], row[ORIGIN_COUNTRY_CODE], row_mode)
            dst = process_city(row[DESTINATION_CITY_CODE], row[DESTINATION_CITY], row[DESTINATION_COUNTRY_CODE], row_mode)
            if not col_motif:
                row['Motif'] = ''
            l1p5_row = {
                '# mission': id,
                'Date de départ': date,
                'Ville de départ': src['name'],
                'Pays de départ': src['iso country'],
                'Ville de destination': dst['name'],
                'Pays de destination': dst['iso country'],
                'Mode de déplacement': row_mode,
                'Nb de personnes dans la voiture': '1',
                'Aller Retour (OUI si identiques, NON si différents)': is_two_way(row),
                'Motif du déplacement (optionnel)': row['Motif'].replace('\n',' - ') if 'Motif' in row else '', 
                'Statut de l\'agent (optionnel)': ''
            }
            oreli_out.append(l1p5_row)
        else:
            print('Structure not found: ', row['Libellé structure bénéficiaire'])

    if len(oreli_out) > 0:
        with open(output_filename, 'w', newline='', encoding='utf8') as csvfile:
            fieldnames = oreli_out[0].keys()
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames, delimiter='\t')
            writer.writeheader()
            for row in oreli_out:
                writer.writerow(row)

if __name__ == '__main__':
    main(sys.argv[1:])
